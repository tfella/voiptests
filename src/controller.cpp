#include "controller.h"

QDebug operator<<(QDebug debug, const Stream& plug)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "Stream(id: " << plug.id << ", opts: " << plug.opts << ')';
    return debug;
}

const QDBusArgument &operator<<(const QDBusArgument &argument, const Stream &/*stream*/)
{
    argument.beginStructure();
//     argument << stream.id << stream.opts;
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, Stream &stream)
{
    argument.beginStructure();
    argument >> stream.id >> stream.opts;
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, QVector<Stream> &stream)
{
    argument.beginArray();
    while ( !argument.atEnd() ) {
        Stream element;
        argument >> element;
        stream.append( element );
    }
    argument.endArray();
    return argument;
}

void Controller::handleStreams(const QVector<Stream> &streams)
{
    const QVariantMap startParameters = {
        { QLatin1String("handle_token"), m_handleToken }
    };

    auto reply = m_interface->OpenPipeWireRemote(m_path, startParameters);
    reply.waitForFinished();

    if (reply.isError()) {
        qWarning() << "Could not start stream" << reply.error();
        exit(1);
        return;
    }

    g_object_set(m_pipewiresrc, "fd", reply.value().fileDescriptor(), nullptr);
    gst_element_set_state(pipeline, GST_STATE_PLAYING);
}

void Controller::init(const QDBusObjectPath &path)
{
    m_path = path;
    const QVariantMap sourcesParameters = {
        { QLatin1String("handle_token"), m_handleToken },
        { QLatin1String("types"), uint(Monitor|Window) },
        { QLatin1String("multiple"), false }, // ?
        { QLatin1String("cursor_mode"), uint(Embedded) }
    };
    auto reply = m_interface->SelectSources(m_path, sourcesParameters);
    reply.waitForFinished();
    if(reply.isError()) {
        qDebug() << "Could not select sources" << reply.error();
        return;
        //TODO fail
    }
}

void Controller::start()
{
    const QVariantMap startParameters = {
        { QLatin1String("handle_token"), m_handleToken }
    };
    auto reply = m_interface->Start(m_path, QStringLiteral("org.kde.neochat"), startParameters);
    reply.waitForFinished();
    
    if(reply.isError()) {
        qDebug() << "Could not start stream" << reply.error();
        return;
        //TODO fail
    }
}

void Controller::response(uint code, const QVariantMap &results)
{
    if(code > 0) {
        qDebug() << "Error:" << code << results;
        //TODO fail
    }
    const auto streamsIt = results.constFind("streams");
    if(streamsIt != results.constEnd()) {
        QVector<Stream> streams;
        streamsIt->value<QDBusArgument>() >> streams;
        handleStreams(streams);
        return;
    }
    const auto handleIt = results.constFind(QStringLiteral("session_handle"));
    if(handleIt != results.constEnd()) {
        init(QDBusObjectPath(handleIt->toString()));
        return;
    }
    if(results.isEmpty()) {
        start();
        return;
    }
}

int Controller::doThings(int argc, char *argv[]) {
    gst_init (&argc, &argv);
    QGuiApplication app(argc, argv);
    gst_element_factory_make("qmlglsink", nullptr);
    qmlRegisterSingletonInstance("org.kde.neochat", 1, 0, "Controller", this);
    m_engine = new QQmlApplicationEngine();
    m_engine->load(QUrl(QStringLiteral("qrc:/main.qml")));
    pipeline = gst_pipeline_new(nullptr);

    m_interface = new OrgFreedesktopPortalScreenCastInterface(QLatin1String("org.freedesktop.portal.Desktop"), QLatin1String("/org/freedesktop/portal/desktop"), QDBusConnection::sessionBus());
    
    m_handleToken = QStringLiteral("NeoChat%1").arg(QRandomGenerator::global()->generate());
    const auto sessionParameters = QVariantMap {
        {QLatin1String("session_handle_token"), m_handleToken},
        {QLatin1String("handle_token"), m_handleToken}
    };
    auto sessionReply = m_interface->CreateSession(sessionParameters);
    sessionReply.waitForFinished();
    
    if(!sessionReply.isValid()) {
        qDebug() << "Could not initialize screen cast backend";
        return -1;
    }
    const bool ret = QDBusConnection::sessionBus().connect(QString(), sessionReply.value().path(), QLatin1String("org.freedesktop.portal.Request"), QLatin1String("Response"), this, SLOT(response(uint, QVariantMap)));

    if(!ret) {
        qDebug() << "Failed to create session";
        return -1;
    }

    qDBusRegisterMetaType<Stream>();
    qDBusRegisterMetaType<QVector<Stream>>();
    
    // Sources
    GstElement *videotestsrc = gst_element_factory_make("videotestsrc", "videotestsrc");
    GstElement *ximagesrc = gst_element_factory_make("ximagesrc", "screenshare");
    m_pipewiresrc = gst_element_factory_make("pipewiresrc", "pipewiresrc");

    // Things
    GstElement *glupload = gst_element_factory_make ("glupload", "glupload");
    GstElement *videoconvert = gst_element_factory_make("videoconvert", "videoconvert");
    GstElement *scale = gst_element_factory_make("videoscale", "videoscale");
    GstElement *glcolorconvert = gst_element_factory_make("glcolorconvert", "glcolorconvert");
    GstElement *glcolorbalance = gst_element_factory_make("glcolorbalance", "glcolorbalance");

    // Sinks
    qmlglsink = gst_element_factory_make ("qmlglsink", "qmlglsink");
    
//     g_object_set(ximagesrc, "use-damage", FALSE, nullptr);
//     g_object_set(ximagesrc, "show-pointer", TRUE, nullptr);
//     g_object_set(ximagesrc, "xid", 0, nullptr);
//     g_object_set(scale, "method", 0, nullptr);

//     GstCaps *caps = gst_caps_new_simple("video/x-raw", "framerate", GST_TYPE_FRACTION, 25, 1, nullptr);
//     GstElement *capsfilter = gst_element_factory_make("capsfilter", nullptr);
//     g_object_set(capsfilter, "caps", caps, nullptr);
//     gst_caps_unref(caps);

    g_object_set(qmlglsink, "widget", m_item, nullptr);
    gst_bin_add_many(GST_BIN(pipeline), m_pipewiresrc, glupload, glcolorconvert, glcolorbalance, qmlglsink, nullptr);
    gst_element_link_many(m_pipewiresrc, glupload, glcolorconvert, glcolorbalance, qmlglsink, nullptr);

    return app.exec();
}
