
#include "controller.h"

int main(int argc, char *argv[])
{
    auto controller = new Controller();
    return controller->doThings(argc, argv);
}
