#pragma once
#include <QObject>
#include <QQuickItem>
#include <gst/gst.h>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QTimer>
#include <gst/gstdebugutils.h>
#include "xdp_dbus_screencast_interface.h"
#include <QDebug>

struct Stream {
    uint id;
    QVariantMap opts;
};
Q_DECLARE_METATYPE(Stream);

class Controller : public QObject
{
    Q_OBJECT
public:
    enum CursorModes {
        Hidden = 1,
        Embedded = 2,
        Metadata = 4,
    };
    Q_ENUM(CursorModes);

    enum SourceTypes {
        Monitor = 1,
        Window = 2,
    };
    Q_ENUM(SourceTypes);

    
    QQuickItem *m_item;
    GstElement *pipeline;
    QDBusObjectPath m_path;
    QString m_handleToken;
    QQmlApplicationEngine *m_engine;
    GstElement *m_pipewiresrc;
    GstElement *qmlglsink;
    OrgFreedesktopPortalScreenCastInterface *m_interface = nullptr;
    Q_INVOKABLE void setVideoItem(QQuickItem *item) {
        m_item = item;
    }

    Controller(QObject *parent = nullptr) {}
    
    void handleStreams(const QVector<Stream> &streams);

    void init(const QDBusObjectPath &path);
    void start();
    int doThings(int argc, char *argv[]);
public Q_SLOTS:
    void response(uint code, const QVariantMap &results);
};
