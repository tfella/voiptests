import QtQuick 2.1
import QtQuick.Controls 2.0
import org.freedesktop.gstreamer.GLVideoItem 1.0
import org.kde.neochat 1.0

ApplicationWindow {
    visible: true
    GstGLVideoItem {
        anchors.fill: parent
        Component.onCompleted: Controller.setVideoItem(this)
    }
}
